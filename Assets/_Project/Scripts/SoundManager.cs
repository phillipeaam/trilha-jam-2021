﻿using UnityEngine;

namespace _Project.Scripts
{
    public sealed class SoundManager : MonoBehaviour
    {
        private void Awake() => DontDestroyOnLoad(gameObject);
    }
}