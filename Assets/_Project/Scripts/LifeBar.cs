﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace _Project.Scripts
{
    public class LifeBar : MonoSingleton<LifeBar>
    {
        [SerializeField] private float _maxBarAmount;
        [SerializeField] private float _delayToDecreaseTime;
        [SerializeField] private float _decreaseAmountOverTime;
        [SerializeField] private float _delayToIncreaseTime;
        [SerializeField] private float _increaseAmountOverTime;
        [SerializeField] private Slider _slider;
        [SerializeField] private UnityEvent _onDie;

        private float _currentBarAmount;
        private Coroutine _currentRoutine;

        private float CurrentBarAmount
        {
            get => _currentBarAmount;
            set
            {
                _currentBarAmount = value;
                UpdateSlider();
                if (_currentBarAmount <= 0) _onDie?.Invoke();
            }
        }

        private void UpdateSlider() => _slider.value = 1F / _maxBarAmount * _currentBarAmount;

        private void Start() => CurrentBarAmount = _maxBarAmount;

        public void StartDecrease()
        {
            StopPreviousRoutine();
            _currentRoutine = StartCoroutine(StartDecreaseRoutine());
        }

        private void StopPreviousRoutine()
        {
            if (_currentRoutine == null) return;
            
            StopCoroutine(_currentRoutine);
            _currentRoutine = null;
        }

        private IEnumerator StartDecreaseRoutine()
        {
            while (CurrentBarAmount > 0)
            {
                yield return new WaitForSeconds(_delayToDecreaseTime);
                CurrentBarAmount -= _decreaseAmountOverTime;
            }
        }

        public void StartIncrease()
        {
            StopPreviousRoutine();
            _currentRoutine = StartCoroutine(StartIncreaseRoutine());
        }
        
        private IEnumerator StartIncreaseRoutine()
        {
            while (CurrentBarAmount < _maxBarAmount)
            {
                yield return new WaitForSeconds(_delayToIncreaseTime);
                CurrentBarAmount += _increaseAmountOverTime;
            }
        }
    }
}