﻿using UnityEngine;

namespace _Project.Scripts
{
    [RequireComponent(typeof(Collider))]
    public class ForceArea : MonoBehaviour
    {
        private const string PlayerTag = "Player";
        
        [SerializeField] private GameObject _directionReference;
        [SerializeField] private float _impulse = 16F;

        private Transform _transform;
        
        private void Awake() => InitComponent();

        private void InitComponent()
        {
            _transform = transform;
            _directionReference.SetActive(false);
            GetComponent<Collider>().isTrigger = true;
        }

        private void OnTriggerStay(Collider other)
        {
            if (!other.CompareTag(PlayerTag)) return;
            
            var movementManager = other.GetComponent<MovementManager>();
            if (movementManager == null) return;
            
            movementManager.ApplyImpulse(_transform.forward * _impulse * Time.deltaTime);
        }
    }
}