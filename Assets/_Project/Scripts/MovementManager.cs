using System.Collections;
using UnityEngine;

namespace _Project.Scripts
{
    [RequireComponent(typeof(Rigidbody))]
    public class MovementManager : MonoBehaviour
    {
        [SerializeField] private float _movementForce = 4F;
        [SerializeField] private float _movementResistance = 1F;
        [SerializeField] private float _maximumVelocity = 4F;
        [SerializeField] private float _rotationSpeed = 1.6F;

        public float MaxVelocity
        {
            get
            {
                return _maximumVelocity;
            }
        }

        public bool CanMove
        {
            get
            {
                return _canMove;
            }
        }

        private bool _canMove;
        private Transform _transform;
        private Rigidbody _rigidbody;
        private Quaternion _endRotation;
        private Coroutine _rotationRoutine;

        private void Awake() => InitVariables();

        private void InitVariables()
        {
            _canMove = true;
            _transform = transform;
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate() => CalculateMovement();

        private void CalculateMovement()
        {
            if (!_canMove) return;

            var velocity = _rigidbody.velocity;

            CalculateDirectionForce(velocity);
            CalculateMovementResistance(velocity);
            CalculateRotation();
        }

        #region CalculateDirectionForce()

        private void CalculateDirectionForce(Vector3 velocity)
        {
            if (velocity.magnitude >= _maximumVelocity) return;

            if (Input.GetKey(KeyCode.W)) AddMovementForce(Vector3.up);

            if (Input.GetKey(KeyCode.S)) AddMovementForce(Vector3.down);

            if (Input.GetKey(KeyCode.D)) AddMovementForce(Vector3.right);

            if (Input.GetKey(KeyCode.A)) AddMovementForce(Vector3.left);
        }

        private void AddMovementForce(Vector3 direction) => _rigidbody.AddForce(_movementForce * direction);

        #endregion

        #region CalculateMovementResistance()

        private void CalculateMovementResistance(Vector3 velocity)
        {
            if (velocity.magnitude == 0) return;

            var resistanceDirection = -velocity.normalized;
            _rigidbody.AddForce(_movementResistance * resistanceDirection);
        }

        #endregion

        #region CalculateRotation()

        private void CalculateRotation()
        {
            var directionToRotate = GetDirectionToRotate();
            if (directionToRotate == Vector3.zero) return;

            Rotate(directionToRotate);
        }

        private Vector3 GetDirectionToRotate()
        {
            var directionToRotate = Vector3.zero;

            if (Input.GetKey(KeyCode.W)) directionToRotate += Vector3.up;
            if (Input.GetKey(KeyCode.S)) directionToRotate += Vector3.down;
            if (Input.GetKey(KeyCode.D)) directionToRotate += Vector3.right;
            if (Input.GetKey(KeyCode.A)) directionToRotate += Vector3.left;
            
            return directionToRotate;
        }

        private void Rotate(Vector3 directionToRotate)
        {
            StopPreviousRotationRoutine();
            _rotationRoutine = StartCoroutine(RotateRoutine(directionToRotate));
        }
        
        private void StopPreviousRotationRoutine()
        {
            if (_rotationRoutine == null) return;

            StopCoroutine(_rotationRoutine);
            _rotationRoutine = null;
        }
        
        private IEnumerator RotateRoutine(Vector3 directionToRotate)
        {
            _endRotation = Quaternion.LookRotation(directionToRotate);

            while (true)
            {
                var startRotation = _transform.rotation;
                if (startRotation == _endRotation)
                {
                    _transform.rotation = _endRotation;
                    _rotationRoutine = null;

                    yield break;
                }

                _transform.rotation = Quaternion.Lerp(startRotation, _endRotation, _rotationSpeed * Time.deltaTime);

                yield return null;
            }
        }

        #endregion

        #region ToggleMovement()

        public void ToggleMovement(bool playerCanMove)
        {
            _canMove = playerCanMove;
            TurnIndefinitely(playerCanMove);
            UpdateLifeStatus();
        }

        private void TurnIndefinitely(bool playerCanMove)
        {
            if (!playerCanMove)
            {
                StopPreviousRotationRoutine();
                //TODO Implementar lógica para que continue a rotação no sentido que estava ocorrendo na animação 
            }
        }

        #endregion

        public void ApplyResistance(float resistancePercentage)
        {
            _rigidbody.AddForce(-_rigidbody.velocity * resistancePercentage, ForceMode.VelocityChange);
        }
        
        public void ApplyImpulse(Vector3 direction) => _rigidbody.AddForce(direction, ForceMode.Impulse);

        private void UpdateLifeStatus()
        {
            if (_canMove) LifeBar.Instance.StartIncrease();
            else LifeBar.Instance.StartDecrease();
        }

        public Vector3 GetInputVector()
        {
            float x = 0;
            if (Input.GetKey(KeyCode.D)) x++;
            if (Input.GetKey(KeyCode.A)) x--;
            float y = 0;
            if (Input.GetKey(KeyCode.W)) y++;
            if (Input.GetKey(KeyCode.S)) y--;
            return new Vector3(x, y,0);
        }
    }
}