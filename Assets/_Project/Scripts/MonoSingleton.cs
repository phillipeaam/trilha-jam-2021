﻿using UnityEngine;

namespace _Project.Scripts
{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
    {
        public static T Instance { get; private set; }

        protected virtual void Awake() => SetSingleton();

        private void SetSingleton()
        {
            if (Instance == null) Instance = this as T;
            else if (Instance != this) Destroy(gameObject);
        }
    }
}