﻿using UnityEngine;

namespace _Project.Scripts
{
    [RequireComponent(typeof(Collider))]
    public class SlimeArea : MonoBehaviour
    {
        private const string PlayerTag = "Player";
        
        [SerializeField] [Range(0, 1)] private float _enterResistancePercentage = 0.8F;

        private void Awake() => InitComponent();

        private void InitComponent() => GetComponent<Collider>().isTrigger = true;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag(PlayerTag)) return;
            
            var movementManager = other.GetComponent<MovementManager>();
            if (movementManager != null) movementManager.ToggleMovement(true);
                
            movementManager.ApplyResistance(_enterResistancePercentage);
        }

        private void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag(PlayerTag)) return;
            
            var movementManager = other.GetComponent<MovementManager>();
            if (movementManager != null) movementManager.ToggleMovement(false);
        }
    }
}