﻿using _Project.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationsScript : MonoBehaviour
{
    //coloquei como serialize field pq o mestre não gosta de GetComponent<>
    [SerializeField] private MovementManager m_MoveComponent;
    [SerializeField] private Animator m_Animator;
    [SerializeField] private Rigidbody m_RigidBody;
    [SerializeField] private float m_SmoothSpeed = 5f;
    [SerializeField] private float m_LookMultiplier = 2f;


    private void Update()
    {
        SetAnimatorSpeed();
        UpdateOutOfWater();
        CalculateLookAlphas();
        SetBreaking();
    }

    #region ToBeCalledEveryframe
    private void SetAnimatorSpeed()
    {
        float newSpeed = Mathf.Abs(m_RigidBody.velocity.magnitude)/m_MoveComponent.MaxVelocity;
        m_Animator.SetFloat("Speed", newSpeed);
    }

    private void UpdateOutOfWater()
    {
        m_Animator.SetBool("Out_Of_Water", !m_MoveComponent.CanMove);
    }

    private void CalculateLookAlphas()
    {
        Vector3 FacingDirection = m_MoveComponent.transform.forward;
        Vector3 InputDirection = m_MoveComponent.GetInputVector();
        float AngleUpDown = 0;
        float AngleLeftRight = 0;

        if (InputDirection != Vector3.zero)
        {
            AngleUpDown = Vector3.SignedAngle(InputDirection, FacingDirection, m_MoveComponent.transform.right);
            AngleLeftRight = Vector3.SignedAngle(FacingDirection, InputDirection, m_MoveComponent.transform.up);
        }

        bool hasUpDownInput = InputDirection.y != 0;
        float targetUp = hasUpDownInput? Mathf.Clamp((AngleUpDown / 180f) *m_LookMultiplier, 0f, 1f) : 0f;
        float currentUp = m_Animator.GetLayerWeight(m_Animator.GetLayerIndex("Look_Up"));
        m_Animator.SetLayerWeight(m_Animator.GetLayerIndex("Look_Up"), Mathf.Lerp(currentUp, targetUp, Time.deltaTime * m_SmoothSpeed));

        float targetDown = hasUpDownInput? Mathf.Clamp((AngleUpDown / -180f) * m_LookMultiplier, 0f, 1f) : 0f;
        float currentDown = m_Animator.GetLayerWeight(m_Animator.GetLayerIndex("Look_Down"));
        m_Animator.SetLayerWeight(m_Animator.GetLayerIndex("Look_Down"), Mathf.Lerp(currentDown, targetDown, Time.deltaTime * m_SmoothSpeed));

        bool hasLeftRightInput = InputDirection.x != 0;
        float targetRight = hasLeftRightInput ? Mathf.Clamp((AngleLeftRight / 180f) * m_LookMultiplier, 0f, 1f) : 0f;
        float currentRight = m_Animator.GetLayerWeight(m_Animator.GetLayerIndex("Look_Right"));
        m_Animator.SetLayerWeight(m_Animator.GetLayerIndex("Look_Right"), Mathf.Lerp(currentRight, targetRight, Time.deltaTime * m_SmoothSpeed));

        float targetLeft = hasLeftRightInput ? Mathf.Clamp((AngleLeftRight / -180f) * m_LookMultiplier, 0f, 1f) : 0f;
        float currentLeft = m_Animator.GetLayerWeight(m_Animator.GetLayerIndex("Look_Left"));
        m_Animator.SetLayerWeight(m_Animator.GetLayerIndex("Look_Left"), Mathf.Lerp(currentLeft, targetLeft, Time.deltaTime * m_SmoothSpeed));
    }

    private void SetBreaking()
    {
        m_Animator.SetBool("Breaking", m_MoveComponent.GetInputVector() == Vector3.zero && Mathf.Abs(m_RigidBody.velocity.magnitude) > 0.7f);
    }
    #endregion

    public void PlayDeadAnimation(bool play = true)
    {
        m_Animator.SetBool("Dead", play);
    }

    public void PlayEating()
    {
        m_Animator.SetTrigger("Eating");
    }
}
