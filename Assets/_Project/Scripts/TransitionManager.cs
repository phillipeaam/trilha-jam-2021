﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace _Project.Scripts
{
    public class TransitionManager : MonoSingleton<TransitionManager>
    {
        [SerializeField] private float _endAnimationDelay;
        [SerializeField] private TMP_Text _message;
        [SerializeField] private Animator _animator;
        
        private static readonly int FinishGame = Animator.StringToHash("FinishGame");

        private void Start()
        {
            Debug.Log($"Level: {SceneManager.GetActiveScene().buildIndex}");
        }

        public void LoadNextLevel(string message)
        {
            _message.text = message;
            _animator.SetTrigger(FinishGame);
            
            var buildIndex = SceneManager.GetActiveScene().buildIndex + 1;
            if (buildIndex > SceneManager.sceneCount + 1) buildIndex = 0;
            
            StartCoroutine(LoadStageRoutine(buildIndex));
        }
        
        public void ResetStage()
        {
            _animator.SetTrigger(FinishGame);
            StartCoroutine(LoadStageRoutine(SceneManager.GetActiveScene().buildIndex));
        }

        private IEnumerator LoadStageRoutine(int buildIndex)
        {
            yield return new WaitForSeconds(_endAnimationDelay);
            SceneManager.LoadScene(buildIndex, LoadSceneMode.Single);
        }
    }
}
