﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

namespace _Project.Scripts
{
    [RequireComponent(typeof(Collider))]
    public class ShrinkArea : MonoBehaviour
    {
        private const string PlayerTag = "Player";

        [SerializeField] private float _delayToStartShrink;
        [SerializeField] private float _timeToCompleteShrink;

        private Transform _transform;
        
        private void Awake() => InitComponent();

        private void InitComponent()
        {
            _transform = transform;
            GetComponent<Collider>().isTrigger = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag(PlayerTag)) return;

            StartCoroutine(ShrinkRoutine());
        }

        private IEnumerator ShrinkRoutine()
        {
            _transform.DOScale(_transform.localScale * 1.1F, _delayToStartShrink);
                
            yield return new WaitForSeconds(_delayToStartShrink);
            
            _transform
                .DOScale(Vector3.zero, _timeToCompleteShrink)
                .OnComplete(() => Destroy(gameObject));
        }
    }
}
