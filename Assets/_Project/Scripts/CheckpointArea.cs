﻿using UnityEngine;

namespace _Project.Scripts
{
    [RequireComponent(typeof(Collider))]
    public class CheckpointArea : MonoBehaviour
    {
        private const string PlayerTag = "Player";

        [SerializeField] private string _message;

        private void Awake() => InitComponent();

        private void InitComponent() => GetComponent<Collider>().isTrigger = true;
        
        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag(PlayerTag)) return;

            TransitionManager.Instance.LoadNextLevel(_message);
        }
    }
}
